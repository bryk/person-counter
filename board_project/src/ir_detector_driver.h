#ifndef __IR_DETECTOR_DRIVER_H
#define __IR_DETECTOR_DRIVER_H

#include "stm32f0xx.h"

uint8_t init_ir_detectors(void);

#endif
