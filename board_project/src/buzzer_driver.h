#ifndef __BUZZER_DRIVER_H
#define __BUZZER_DRIVER_H

#include "stm32f0xx.h"

void init_buzzer(void);
void buzz(void);
void buzz2(void);
void buzzer_poll(void);
void buzz_error(void);

#endif
