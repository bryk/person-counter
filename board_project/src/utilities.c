#include "utilities.h"
#include "stm32f0_discovery.h"
#include <string.h>

static __IO uint64_t time;

void init_timer(void) {
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config(RCC_Clocks.HCLK_Frequency / 10000);
}

void init_board_utilities(void) {
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDOn(LED3);
}

static __IO uint8_t val = 1;

void toggle_led(void) {
	if(val) { 
	  STM_EVAL_LEDOff(LED3);
	} else {
	  STM_EVAL_LEDOn(LED3);
	}
	val = !val;
}

uint64_t current_time(void) {
	return time;
}

void sleep_ms(uint32_t ms) {
	uint64_t time = current_time();
	uint64_t to_wait = ms * 10;
	while(current_time() - time < to_wait) {
	}
}

void on_systick_passed(void) {
  time = (time + 1) % 1000000000;
}

void reverse(char s[]) {
	int i, j;
  char c;
  for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
    c = s[i];
    s[i] = s[j];
    s[j] = c;
  }
}

void itoa(int n, char s[]) {
  int i, sign;
  if ((sign = n) < 0) 
    n = -n;        
  i = 0;
  do {       
    s[i++] = n % 10 + '0';
  } while ((n /= 10) > 0);
  if (sign < 0)
    s[i++] = '-';
  s[i] = '\0';
  reverse(s);
}
