#ifndef __UART_H
#define __UART_H

#include "stm32f0xx.h"
#include "stm32f0_discovery.h"

void uart_configure(void);
void uart_printf(char * fmt, ...);
void uart_send_string(char *c);
void uart_send_byte(char c);
void uart_poll(void);

#endif /* __UART_H */
