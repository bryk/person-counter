#ifndef __IR_LEF_DRIVER_H
#define __IR_LEF_DRIVER_H

#include "stm32f0xx.h"

void init_ir_leds(void);
void ir_leds_pool(void);

#endif
