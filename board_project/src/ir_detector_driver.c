#include "ir_detector_driver.h"
#include "uart.h"
#include "utilities.h"
#include "buzzer_driver.h"
#include "lcd_driver.h"
#include "string.h"
#include <string.h>

static __IO int32_t last_time = 0;
static __IO uint32_t last_visible_yellow = 0; // PB14
static __IO uint32_t last_visible_red = 0; // PB15
static __IO int32_t n_people = 0;
static __IO uint64_t last_detection = 0;
static __IO uint64_t iteration = 0;

#define RED_PIN GPIO_Pin_15
#define YELLOW_PIN GPIO_Pin_14
#define RED_PIN_SOURCE EXTI_PinSource15
#define YELLOW_PIN_SOURCE EXTI_PinSource14
#define YELLOW_EXTI EXTI_Line14
#define RED_EXTI EXTI_Line15



#define NOT_VISIBLE(x) (x == 1)
#define VISIBLE(x) (x == 0)

uint8_t init_values(void) {
	uint8_t yellow_state = GPIO_ReadInputDataBit(GPIOB, YELLOW_PIN);
	uint8_t red_state = GPIO_ReadInputDataBit(GPIOB, RED_PIN);
	uint32_t time = current_time();
	uint8_t ret = 1;
	if(VISIBLE(yellow_state)) {
		last_visible_yellow = time;
		uart_send_string("Yellow detector visible\n");
	} else {
		uart_send_string("Yellow detector not visible!\n");
		ret = 0;
	}
	if(VISIBLE(red_state)) {
		last_visible_red = time;
		uart_send_string("Red detector visible\n");
	} else {
		uart_send_string("Red detector not visible!\n");
		ret = 0;
	}
	if(!ret) {
	  lcd_send_string("Detector or LED\nmisconfigured");
	}
	return ret;
}

uint8_t init_ir_detectors(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	
	  /* Configure Button pin as input */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Pin = RED_PIN | YELLOW_PIN;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	sleep_ms(5);
	if(!init_values()) {
		return 0;
	}
	sleep_ms(5);
	
	/* Connect Button EXTI Line to Button GPIO Pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, RED_PIN_SOURCE);
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, YELLOW_PIN_SOURCE);

  /* Configure Button EXTI line */
  EXTI_InitStructure.EXTI_Line = YELLOW_EXTI | RED_EXTI;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;    
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set Button EXTI Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
	sleep_ms(50);
	
	return 1;
}

void fill_msg(char *buf, int8_t add) {
	n_people += add;
	if(n_people < 0) {
		n_people = 0;
		buzz_error();
		uart_send_string("Inconsistence with IN and OUT detections\n");
	} else if(add >= 1) {
		buzz();
	} else if(add <= -1) {
    buzz2();
	}
	strcpy(buf, "Num people: ");
	buf += strlen(buf);
	itoa(n_people, buf);
}

char msg_buf[16];

void EXTI4_15_IRQHandler(void) {
	uint8_t yellow_state = GPIO_ReadInputDataBit(GPIOB, YELLOW_PIN);
	uint8_t red_state = GPIO_ReadInputDataBit(GPIOB, RED_PIN);
	uint32_t time = current_time();
	iteration++;
			
	if(time - last_detection > 6000) {
		/*if(VISIBLE(yellow_state)) {
			uart_send_string("Y");
		} else {
			uart_send_string("y");
		}
		
		if(VISIBLE(red_state)) {
			uart_send_string("R");
		} else {
			uart_send_string("r");
		}*/
		if(VISIBLE(yellow_state) && last_visible_yellow == 0) {
			last_visible_yellow = iteration;
		}
		if(VISIBLE(red_state) && last_visible_red == 0) {
			last_visible_red = iteration;
		}
		
		if( last_visible_yellow != 0 && last_visible_red != 0) {
			if(last_visible_yellow > last_visible_red) {
				fill_msg(msg_buf, 1);
				lcd_send_string(msg_buf);
				uart_send_string(msg_buf);
			} else if(last_visible_yellow < last_visible_red) {
				fill_msg(msg_buf, -1);
				lcd_send_string(msg_buf);
				uart_send_string(msg_buf);
			} else {
				uart_send_string("Ignoring detection");
			}
			last_visible_yellow = 0;
			last_visible_red = 0;
			last_detection = time;
		}
  }
	toggle_led();
	EXTI_ClearITPendingBit(YELLOW_EXTI);
	EXTI_ClearITPendingBit(RED_EXTI);
}
