#ifndef __UTILITIES_H
#define __UTILITIES_H

#include "stm32f0xx.h"

void init_timer(void);
void init_board_utilities(void);
void toggle_led(void);
void on_systick_passed(void);
uint64_t current_time(void);
void sleep_ms(uint32_t ms);
void itoa(int n, char s[]);

#define GPIO_HIGH(a,b) 		a->BSRR = b
#define GPIO_LOW(a,b)		a->BRR = b
#define GPIO_TOGGLE(a,b) 	a->ODR ^= b

#endif /* __UTILITIES_H */
