#include "uart.h"
#include "messages.h"
#include <stdio.h>
#include <stdarg.h>

#define UART_BUFSIZ 0xff
char tx_buf[UART_BUFSIZ]; 
char sent_buf[UART_BUFSIZ];
static __IO uint8_t to_send = 0;
static __IO uint8_t pos = 0;
static __IO uint8_t mutex = 0;


void interrupts_configure(void);
void uart_initialize(void);

void uart_poll(void) {
  while(to_send) {
    USART_SendData(USART1, (uint8_t) sent_buf[pos]);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET) {}
		to_send--;
		pos = (pos + 1) % UART_BUFSIZ;
	}
}

/**
 * @brief Configures UART. PA9 Tx, PA10 Rx.
 */
void uart_configure(void) {
  interrupts_configure();
  uart_initialize();

  // Enable the USART1 Receive interrupt: this interrupt is generated when the 
  // USART1 receive data register is not empty
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}

/**
 * @brief Configures the nested vectored interrupt controller (NVIC)
 */
void interrupts_configure(void) {
  NVIC_InitTypeDef NVIC_InitStructure;

  // Enable the USART1 Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/**
 * @brief Configures the COM port communication. PA9 Tx, PA10 Rx.
 */
void uart_initialize(void) {
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;

  USART_InitStructure.USART_BaudRate = 38400;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  // Connect PXx to USARTx_Tx
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1);

  // Connect PXx to USARTx_Rx
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);

  // Configure USART Tx, Rx
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  // USART configuration
  USART_Init(USART1, &USART_InitStructure);

  // Enable USART
  USART_Cmd(USART1, ENABLE);
}

/**
 * @brief Sends a byte through UART
 */
void uart_send_byte(char c) {
	uint8_t cur_pos;
	cur_pos = (pos + to_send) % UART_BUFSIZ;
	sent_buf[cur_pos] = c;
	to_send++;
}

/**
 * @brief Sends a string through UART
 */
void uart_send_string(char *c) {
  uint8_t pos = 0;
  while(c[pos] != 0) {
    uart_send_byte(c[pos]);
		pos++;
  }
}

/**
 * @brief printf equivalent for UART
 */
void uart_printf(char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vsprintf(tx_buf, fmt, args);
	uart_send_string(tx_buf);
  va_end(args);
}

/**
 * @brief  This function handles USART1 global interrupt request.
 */
void USART1_IRQHandler(void) {
  if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
    // read the byte
    char c = (USART_ReceiveData(USART1) & 0x7F);

    //and echo it
    uart_send_byte(c);
  }
}
