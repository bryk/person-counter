#include "main.h"
#include "uart.h"
#include "ir_led_driver.h"
#include "ir_detector_driver.h"
#include "utilities.h"
#include "buzzer_driver.h"
#include "lcd_driver.h"

void RCC_Conf(void) {
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC, ENABLE); 
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); 
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
}

int main() {
	uint8_t err = 0;
	RCC_Conf();
	init_timer();
	init_board_utilities();
	init_buzzer();
	uart_configure();
	lcd_init();
	init_ir_leds();
	sleep_ms(5);
	if(init_ir_detectors()) {
	  uart_send_string("Person counter started");
	  lcd_send_string("Num people: 0");
	} else {
		buzz_error();
		buzz_error();
		buzz_error();
		err = 1;
	}
	while(1) {
		uart_poll();
		lcd_poll();
	  buzzer_poll();
		ir_leds_pool();
		if(err) {
			sleep_ms(100);
			toggle_led();
			sleep_ms(100);
		}
  }
}
