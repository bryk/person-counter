#include "buzzer_driver.h"
#include "main.h"
#include "utilities.h"

static __IO uint8_t should_buzz = 0;
static __IO uint8_t should_buzz2 = 0;
static __IO uint8_t should_buzz_error = 0;


void buzz(void) {
	should_buzz += 1;
}

void buzz2(void) {
	should_buzz2 += 1;
}

void buzz_error(void) {
	should_buzz_error += 1;
}

void buzzer_poll(void) {
	if(should_buzz) {
		TIM_Cmd(TIM2, ENABLE);
		sleep_ms(150);
		TIM_Cmd(TIM2, DISABLE);
		should_buzz -= 1;
  }
	if(should_buzz2) {
		TIM_Cmd(TIM2, ENABLE);
		sleep_ms(50);
		TIM_Cmd(TIM2, DISABLE);
		sleep_ms(50);
		TIM_Cmd(TIM2, ENABLE);
		sleep_ms(50);
		TIM_Cmd(TIM2, DISABLE);
		should_buzz2 -= 1;
  }
	if(should_buzz_error) {
		TIM_Cmd(TIM2, ENABLE);
		sleep_ms(150);
		TIM_Cmd(TIM2, DISABLE);
		sleep_ms(150);
		TIM_Cmd(TIM2, ENABLE);
		sleep_ms(150);
		TIM_Cmd(TIM2, DISABLE);
		should_buzz_error -= 1;
  }
}

void init_buzzer(void) {
	GPIO_InitTypeDef         GPIO_InitStructure;  
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  TIM_OCInitTypeDef        TIM_OCInitStructure;
	uint16_t period;
  uint16_t pulse;  
	
  /* Configure PA3 pin as TIM2 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure); 
	
  /* Connect TIM2 Channels to PA3 Alternate Function 2 */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_2);
	
	// base freq = 72Mhz / 720 = 100 KHz
  period = 100; // 100 KHz / 100 = 1KHz
  pulse = period / 2; // 1KHz / 2 = 500 Hz

  /* Time Base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = 720;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_CenterAligned1;
  TIM_TimeBaseStructure.TIM_Period = period;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
	TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
	TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;
  TIM_OCInitStructure.TIM_Pulse = pulse;
  TIM_OC4Init(TIM2, &TIM_OCInitStructure);
}
