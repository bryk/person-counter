#include "lcd_driver.h"
#include "utilities.h"

#define LCD_GPIO GPIOC
#define LCD_D4 GPIO_Pin_0
#define LCD_D5 GPIO_Pin_1
#define LCD_D6 GPIO_Pin_2
#define LCD_D7 GPIO_Pin_3
#define LCD_RS GPIO_Pin_4
#define LCD_EN GPIO_Pin_5


#define LCD_BUFSIZ 0xff
char lcd_buf[LCD_BUFSIZ];
__IO uint8_t lcd_to_send = 0;
__IO uint8_t lcd_pos = 0;

void LCD_WriteCommand(u8 commandToWrite);
void LCD_GoTo(unsigned char x, unsigned char y);
void LCD_WriteData(u8 dataToWrite);

void lcd_poll(void) {
	while(lcd_to_send) {
	  if(lcd_buf[lcd_pos] == 0) {
			LCD_WriteCommand((uint8_t)HD44780_CLEAR); //clear
			LCD_GoTo(0, 0);
		} else if(lcd_buf[lcd_pos] == '\n') {
			LCD_GoTo(0, 1);
		}	else {
			LCD_WriteData((uint8_t) lcd_buf[lcd_pos]);
		}
		lcd_to_send--;
		lcd_pos = (lcd_pos + 1) % LCD_BUFSIZ;
	}
}

void lcd_send_byte(char c) {
	uint8_t cur_pos;
	cur_pos = (lcd_pos + lcd_to_send) % LCD_BUFSIZ;
	lcd_buf[cur_pos] = c;
	lcd_to_send++;
}

void lcd_send_string(char *c) {
  uint8_t pos = 0;
  lcd_send_byte(0);
  while(c[pos] != 0) {
    lcd_send_byte(c[pos]);
		pos++;
  }
}

void LCD_WriteNibble(u8 nibbleToWrite)
{
	GPIO_WriteBit(LCD_GPIO, LCD_EN, Bit_SET);
	GPIO_WriteBit(LCD_GPIO, LCD_D4, (nibbleToWrite & 0x01) ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(LCD_GPIO, LCD_D5, (nibbleToWrite & 0x02) ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(LCD_GPIO, LCD_D6, (nibbleToWrite & 0x04) ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(LCD_GPIO, LCD_D7, (nibbleToWrite & 0x08) ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(LCD_GPIO, LCD_EN, Bit_RESET);
}


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void LCD_WriteData(u8 dataToWrite)
{
	GPIO_WriteBit(LCD_GPIO, LCD_RS, Bit_SET);
	
	LCD_WriteNibble(dataToWrite >> 4);
	LCD_WriteNibble(dataToWrite & 0x0F);
	
	sleep_ms(1);
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void LCD_WriteCommand(u8 commandToWrite)
{
	GPIO_WriteBit(LCD_GPIO, LCD_RS, Bit_RESET);
	LCD_WriteNibble(commandToWrite >> 4);
	LCD_WriteNibble(commandToWrite & 0x0F);
	sleep_ms(1);
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void LCD_WriteText(char * text)
{
	while(*text)
	  LCD_WriteData(*text++);
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void LCD_GoTo(unsigned char x, unsigned char y)
{
	LCD_WriteCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void LCD_WriteTextXY(char * text, u8 x, u8 y)
{
	LCD_GoTo(x,y);
	while(*text)
	  LCD_WriteData(*text++);
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void LCD_ShiftLeft(void)
{
	LCD_WriteCommand(HD44780_DISPLAY_CURSOR_SHIFT | HD44780_SHIFT_LEFT | HD44780_SHIFT_DISPLAY);
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void LCD_ShiftRight(void)
{
	LCD_WriteCommand(HD44780_DISPLAY_CURSOR_SHIFT | HD44780_SHIFT_RIGHT | HD44780_SHIFT_DISPLAY);
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void lcd_init(void)
{
	vu8 i = 0;
  GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin   =  LCD_D4|LCD_D5|LCD_D6|LCD_D7|LCD_RS|LCD_EN;
	GPIO_InitStructure.GPIO_Mode  =  GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed =  GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType =  GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  =  GPIO_PuPd_UP;
	
	GPIO_Init(LCD_GPIO, &GPIO_InitStructure);
	
	GPIO_ResetBits(LCD_GPIO, LCD_RS | LCD_EN);
	
	sleep_ms(10);
	
	for(i = 0; i < 3; i++) {
		LCD_WriteNibble(0x03);            
	  sleep_ms(10);
	}
	
	LCD_WriteNibble(0x02);             
	sleep_ms(10);
	   
	LCD_WriteCommand(HD44780_FUNCTION_SET | 
	                 HD44780_FONT5x7 | 
	                 HD44780_TWO_LINE | 
	                 HD44780_4_BIT);
	
	LCD_WriteCommand(HD44780_DISPLAY_ONOFF | 
	                 HD44780_DISPLAY_OFF); 
	
	LCD_WriteCommand(HD44780_CLEAR); 
	
	LCD_WriteCommand(HD44780_ENTRY_MODE | 
	                 HD44780_EM_SHIFT_CURSOR | 
	                 HD44780_EM_INCREMENT);
	
	LCD_WriteCommand(HD44780_DISPLAY_ONOFF | 
	                 HD44780_DISPLAY_ON |
	                 HD44780_CURSOR_OFF | 
	                 HD44780_CURSOR_NOBLINK);
}
