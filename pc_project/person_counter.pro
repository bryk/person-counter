QT += core gui

TARGET = person_counter
TEMPLATE = app

INCLUDEPATH += /usr/include/qt4/QtExtSerialPort \
               include \
               ../common \

SOURCES += src/main.cc \
           src/main_window.cc \
           src/logger.cc \
           ../common/messages.c \

HEADERS += include/main_window.h \
           include/logger.h \
           ../common/messages.h \

FORMS += ui/main_window.ui \

LIBS += -lqextserialport-1.2

RESOURCES += \
    ui/resources/icon.qrc
