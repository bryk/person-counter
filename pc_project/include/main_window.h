#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QDesktopWidget>
#include <QDebug>
#include <QCloseEvent>
#include <QSettings>

#include "qextserialenumerator.h"
#include "qextserialport.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow * ui;
    QextSerialPort * serial_port;

    void writeSettings();
    void loadSettings();
    void initGUI();
    void setConnectionItemsEnabled(bool enabled);

private slots:
    void portComboListChanged(QString);
    void refreshList();
    void portConnect();
    void portDisconnect();
    void onDataAvailable();
    void sendData();
    void clearTerminal();

signals:
    void portInfo(QextPortInfo portInfo);
};

#endif // MAIN_WINDOW_H
