#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include <QDebug>
#include <QDateTime>
#include <QDataStream>

#define SET_DEBUG_LEVEL(x)  Logger::getInstance().setDebugLev(x);
#define LOG_DEBUG(x) Logger::getInstance().logDebug(QString("%1# %2").arg(Q_FUNC_INFO).arg(x));
#define LOG_INFO(x) Logger::getInstance().logInfo(QString("%1# %2").arg(Q_FUNC_INFO).arg(x));
#define LOG_ERROR(x) Logger::getInstance().logError(QString("%1# %2").arg(Q_FUNC_INFO).arg(x));

typedef enum debug_level {
    DEBUG_LEVEL_ERROR,
    DEBUG_LEVEL_INFO,
    DEBUG_LEVEL_DEBUG
} DEBUG_LEVEL;

//Singleton
class Logger : public QObject {
    Q_OBJECT
private:
    Logger(QObject *parent = 0);
    static QString aLogfilePath;
    DEBUG_LEVEL aDebugLev;
    QFile aLogfile;

public:
    static Logger & getInstance() {
      static Logger instance;
      return instance;
    }

    void logDebug(QString msg);
    void logInfo(QString msg);
    void logError(QString msg);
    void setDebugLev(DEBUG_LEVEL debugLev);
};

#endif
