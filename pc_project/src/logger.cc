#include "logger.h"

Logger::Logger(QObject *parent) : QObject(parent) {
    this->setDebugLev(DEBUG_LEVEL_INFO);
}

void Logger::logDebug(QString msg) {
    if (this->aDebugLev < DEBUG_LEVEL_DEBUG) {
        return;
    }

    QString messageToWrite= "DEBUG ";
    messageToWrite += msg;
    qDebug() << messageToWrite;
}

void Logger::logInfo(QString msg) {
    if (this->aDebugLev < DEBUG_LEVEL_INFO) {
        return;
    }

    QString messageToWrite= "INFO ";
    messageToWrite += msg;
    qDebug() << messageToWrite;
}

void Logger::logError(QString msg) {
    QString messageToWrite= "ERROR ";
    messageToWrite += msg;
    qDebug() << messageToWrite;
}

void Logger::setDebugLev(DEBUG_LEVEL debugLev) {
    this->aDebugLev = debugLev;
}
