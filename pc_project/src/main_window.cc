#include "logger.h"
#include "main_window.h"
#include "ui_main_window.h"
#include <QScrollBar>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow) {
  LOG_INFO("Creating user interface");

  ui->setupUi(this);

  // Center the window
  QRect frect = frameGeometry();
  frect.moveCenter(QDesktopWidget().availableGeometry().center());
  move(frect.topLeft());

  connect(this, SIGNAL(portInfo(QextPortInfo)), this, SLOT(aquirePortInfo(QextPortInfo)));
  connect(ui->portsCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(portComboListChanged(QString)));
  connect(ui->connectButton, SIGNAL(clicked()), this, SLOT(portConnect()));
  connect(ui->refreshButton, SIGNAL(clicked()), this, SLOT(refreshList()));
  connect(ui->clearButton, SIGNAL(clicked()), this, SLOT(clearTerminal()));
  connect(ui->sendDataButton, SIGNAL(clicked()), this, SLOT(sendData()));

  this->initGUI();
  serial_port = NULL;
}

void MainWindow::initGUI() {
  this->refreshList();
  //    ui->dataLineEdit->setText("Connect to the database!");
  //    ui->passwordLineEdit->setEchoMode(QLineEdit::Password);
  //    ui->openPortButton->setDisabled(0);
}

void MainWindow::clearTerminal() {
  this->ui->receivedData->clear();
}

void MainWindow::sendData() {
  LOG_INFO(QString("Sending data: %1").arg(ui->sendDataInput->text()));
  serial_port->write(ui->sendDataInput->text().toAscii());
}

void MainWindow::setConnectionItemsEnabled(bool enabled) {
  this->ui->portsCombo->setEnabled(enabled);
  this->ui->refreshButton->setEnabled(enabled);
  this->ui->clearButton->setEnabled(!enabled);
  this->ui->sendDataInput->setEnabled(!enabled);
  this->ui->sendDataButton->setEnabled(!enabled);
  this->ui->receivedData->setEnabled(!enabled);
}

void MainWindow::portComboListChanged(QString portname) {
  QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();

  for (int i = 0; i < ports.size(); i++) {
    if (ports.at(i).portName.toLocal8Bit().constData() == portname) {
      emit portInfo(ports.at(i));
    }
  }
}

void MainWindow::portConnect() {
  LOG_INFO(QString("Connecting"));
  setConnectionItemsEnabled(false);
  disconnect(ui->connectButton, SIGNAL(clicked()), this, SLOT(portConnect()));

  QString port_name = ui->portsCombo->currentText();

  serial_port = new QextSerialPort(port_name, QextSerialPort::EventDriven);
  serial_port->setBaudRate(BAUD38400);
  serial_port->setFlowControl(FLOW_OFF);
  serial_port->setParity(PAR_NONE);
  serial_port->setDataBits(DATA_8);
  serial_port->setStopBits(STOP_1);
  serial_port->open(QIODevice::ReadWrite);

  if(serial_port->isOpen() && serial_port->isReadable()) {
    connect(serial_port, SIGNAL(readyRead()), this, SLOT(onDataAvailable()));
    LOG_INFO(QString("Opened port %1 for reading and writing").arg(port_name));
    ui->connectButton->setText("Disconnect");
  }
  connect(ui->connectButton, SIGNAL(clicked()), this, SLOT(portDisconnect()));
}

void MainWindow::onDataAvailable() {
  int avail = serial_port->bytesAvailable();
  if(avail > 0) {
    QByteArray usbdata;
    usbdata.resize(avail);
    int read = serial_port->read(usbdata.data(), usbdata.size());
    if(read > 0) {
      QString temp(usbdata);
      LOG_INFO(QString("Read data: %1").arg(temp));
      ui->receivedData->setPlainText(ui->receivedData->toPlainText() + temp + "\n");
      QScrollBar * sb = ui->receivedData->verticalScrollBar();
      sb->setValue(sb->maximum());
    }
  }
}

void MainWindow::portDisconnect() {
    LOG_INFO(QString("Disconnecting"));
  setConnectionItemsEnabled(true);
  ui->connectButton->setText("Connect");
  disconnect(ui->connectButton, SIGNAL(clicked()), this, SLOT(portDisconnect()));
  connect(ui->connectButton, SIGNAL(clicked()), this, SLOT(portConnect()));
  disconnect(serial_port, SIGNAL(readyRead()), this, SLOT(onDataAvailable()));
  serial_port->close();
  delete serial_port;
  serial_port = NULL;
}

/**
  Compares port names so that ttyUSB0 is on the top and then ttyS1 ... tty S30.
  */
bool comparePortNames(const QextPortInfo &a, const QextPortInfo &b) {
  if(a.portName.startsWith("ttyS") and b.portName.startsWith("ttyS")) {
    QString tail_a = a.portName.right(a.portName.size() - 4);
    QString tail_b = b.portName.right(b.portName.size() - 4);
    bool ok = true;
    int num_a = tail_a.toInt(&ok);
    if(!ok) {
      return tail_a > tail_b;
    }
    int num_b = tail_b.toInt(&ok);
    if(!ok) {
      return tail_a > tail_b;
    }
    return num_a < num_b;
  } else {
    return a.portName > b.portName;
  }
}

void MainWindow::refreshList() {

  ui->portsCombo->clear();

  QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();
  qSort(ports.begin(), ports.end(), comparePortNames);

  for (int i = 0; i < ports.size(); i++) {
    LOG_INFO(QString("Found port: %1").arg(ports.at(i).portName));
    ui->portsCombo->addItem(ports.at(i).portName.toLocal8Bit().constData());
  }
}

MainWindow::~MainWindow() {
  if(serial_port != NULL) {
    serial_port->close();
    serial_port = NULL;
  }
}

void MainWindow::writeSettings()  {
  QSettings settings("MySoft", "FTDI2MySQL");
  settings.beginGroup("database");
  //    settings.setValue("host", ui->hostLineEdit->text());
  //    settings.setValue("user", ui->userLineEdit->text());
  //    settings.setValue("database", ui->databaseLineEdit->text());
  //    settings.setValue("port", ui->portLineEdit->text());
  settings.endGroup();
}

void MainWindow::loadSettings() {
  QSettings settings("MySoft", "FTDI2MySQL");
  settings.beginGroup("database");
  //    this->ui->hostLineEdit->setText(settings.value("host").toString());
  //    this->ui->userLineEdit->setText(settings.value("user").toString());
  //    this->ui->databaseLineEdit->setText(settings.value("database").toString());
  //    this->ui->portLineEdit->setText(settings.value("port").toString());
  settings.endGroup();
}

//void MainWindow::testDatabase() {
  //    mysqldb.setOptions(ui->hostLineEdit->text(),ui->userLineEdit->text(),
  //                       ui->databaseLineEdit->text(),ui->portLineEdit->text().toInt(),
  //                       ui->passwordLineEdit->text());
  //    if(mysqldb.testConnection()) {
  //        ui->resultLabel->setText("Connected! Now open the port");
  //        ui->openPortButton->setEnabled(1);
  //    }
  //    else
  //        ui->resultLabel->setText("Not connected!");
//}
