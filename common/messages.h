#ifndef _MESSAGES_H
#define _MESSAGES_H

// Message types definitions
#define DETECTOR1_STATE 0x01
#define DETECTOR2_STATE 0x02

typedef struct _message {
  char valid; // 1 if parse_buffer found a message
              // 2 if there was an error
              // 0 on success
  char type;
  char value;
} message;

// How do we send messages through the wire?
// Each message consists of three bytes:
//   1st byte is '~' ASCII char (message start)
//   2nd byte is message type (e.g. DETECTOR1_STATE)
//   3rd byte is the message value

// Returns new message instance with field valid=1 if
// a message in the buffer was found.
// Field valid=0 if message was not found.
// Field valid=2 if the buffer is corrupted.
message parse_buffer(char *buf, int sz);

#endif // _MESSAGES_H
