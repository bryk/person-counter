#include "messages.h"

message parse_buffer(char *buf, int sz) {
  message msg;
  msg.valid = 0;
  if(sz < 3) {
    return msg;
  } else {
    if(buf[0] == '~') {
      msg.valid = 1;
      msg.type = buf[1];
      msg.value = buf[2];
    } else {
      msg.valid = 2; //error
    }
    return msg;
  }
}

